<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    protected $table = 'orders';

    public static function get_order_status($status){
    	$statuses = ['Pending','In Progress','Completed','Declined'];
    	return $statuses[$status];
    }
}
