<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantProducts extends Model
{
    //
    protected $table = 'restaurant_products';
}
