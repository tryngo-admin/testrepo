<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    protected $table = 'users';

    public static function get_status($status)
    {
    	$statuses = ['DeActivated','Activated', 'Deleted'];
    	return $statuses[$status];
    }
}
