<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','UserController@login')->name('login');
Route::post('login_submitted','UserController@login_submitted')->name('login_submitted');

Route::group(['middleware'=>['isLogin']],function(){
	Route::get('users','UserController@users')->name('users');
	Route::get('drivers','UserController@drivers')->name('drivers');
	Route::get('profile','UserController@profile')->name('profile');
	Route::get('dashboard','UserController@dashboard')->name('dashboard');
	Route::get('user_details','UserController@user_details')->name('user_details');
	Route::get('user_images','UserController@user_images')->name('user_images');
	Route::get('logout','UserController@logout')->name('logout');
	Route::get('change_user_status','UserController@change_user_status')->name('change_user_status');
	Route::post('update_profile','UserController@update_profile')->name('update_profile');

	Route::get('restaurants','RestaurantController@restaurants')->name('restaurants');
	Route::get('add_restaurant','RestaurantController@add_restaurant')->name('add_restaurant');
	Route::get('edit_restaurant','RestaurantController@edit_restaurant')->name('edit_restaurant');
	Route::post('restaurant_submitted','RestaurantController@restaurant_submitted')->name('restaurant_submitted');
	Route::get('change_restaurant_status','RestaurantController@change_restaurant_status')->name('change_restaurant_status');
	
	Route::get('change_ecommerce_status','EcommerceController@change_ecommerce_status')->name('change_ecommerce_status');
	Route::get('ecommerce','EcommerceController@ecommerce')->name('ecommerce');
	Route::get('add_ecommerce','EcommerceController@add_ecommerce')->name('add_ecommerce');
	Route::get('edit_ecommerce','EcommerceController@edit_ecommerce')->name('edit_ecommerce');
	Route::post('ecommerce_submitted','EcommerceController@ecommerce_submitted')->name('ecommerce_submitted');

	Route::get('categories','CategoriesController@categories')->name('categories');
	Route::get('add_category','CategoriesController@add_category')->name('add_category');
	Route::get('edit_category','CategoriesController@edit_category')->name('edit_category');
	Route::post('category_submitted','CategoriesController@category_submitted')->name('category_submitted');
	Route::get('main_categories','CategoriesController@main_categories')->name('main_categories');
	Route::get('change_category_status','CategoriesController@change_category_status')->name('change_category_status');

	Route::get('ecommerce_products','ProductController@ecommerce_products')->name('ecommerce_products');
	Route::get('restaurant_products','ProductController@restaurant_products')->name('restaurant_products');
	Route::get('change_product_status','ProductController@change_product_status')->name('change_product_status');
	Route::get('add_product','ProductController@add_product')->name('add_product');
	Route::get('edit_product','ProductController@edit_product')->name('edit_product');
	Route::get('restaurant_r_ecommerce','ProductController@restaurant_r_ecommerce')->name('restaurant_r_ecommerce');
	Route::get('restaurant_r_ecommerce_categories','ProductController@restaurant_r_ecommerce_categories')->name('restaurant_r_ecommerce_categories');
	Route::post('product_submitted','ProductController@product_submitted')->name('product_submitted');

	Route::get('orders','OrderController@orders')->name('orders');




});



// Route::get('/', function () {
//     return view('welcome');
// });


