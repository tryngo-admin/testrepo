<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('index', 'Api\TestApiController@index');

//------------------------------
Route::post('mobilesignup', 'Api\MobileController@mobileSignUp');

Route::post('mobilelogin', 'Api\MobileController@mobilelogin');

Route::post('mobilelogout', 'Api\MobileController@mobilelogout');

Route::post('sendMobileOtp', 'Api\MobileController@sendMobileOtp');

Route::post('otpConfirmViaSms', 'Api\MobileController@otpConfirmViaSms');


// Route::post('emailverify', 'Api\MobileController@otpConfirmViaEmail');

/* Change/Reset Password */
Route::post('forgotpassword', 'Api\MobileController@forgetPassword');

Route::post('forgotpasswordotp', 'Api\MobileController@matchforgotpasswordCode');

Route::post('updatePassword', 'Api\UserController@updatePassword');

Route::post('getUserInfo', 'Api\UserController@getUserInfo');

Route::post('updateUserDetails', 'Api\UserController@userDetailsUpdate');

Route::post('addProfilePicture', 'Api\UserController@addProfilePicture'); //add and update profile picture

Route::post('removeProfilePicture', 'Api\UserController@removeProfilePicture');

Route::post('userOrderList', 'Api\OrderController@userOrderList');

Route::post('driverDashboardOrderList', 'Api\OrderController@driverDashboardOrderList');

Route::post('addToCart', 'Api\OrderController@addToCart');

Route::post('removeFromCart', 'Api\OrderController@removeFromCart');

Route::post('updateCart', 'Api\OrderController@updateCart');

Route::post('userCartList', 'Api\OrderController@userCartList');

Route::post('getOrderDetail', 'Api\OrderController@getOrderDetail');

Route::post('getAllRestaurants','Api\RapidCarApp@getAllRestaurants');

Route::post('getAllCategories','Api\RapidCarApp@getAllCategories');

Route::post('getCategoryProducts','Api\RapidCarApp@getCategoryProducts');

Route::post('restaurantAllProducts','Api\RapidCarApp@restaurantAllProducts');

