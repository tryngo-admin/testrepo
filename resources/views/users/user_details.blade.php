@extends('layouts.users')
@section('content')
    <div class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title mt-0">User Details</h4>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3"
                                         style="box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgb(0 0 0 / 32%); padding: 15px 0px; border-radius: 8px;">
                                        <div style="text-align: -webkit-center;">
                                            <img class="rounded-circle"
                                                 src="{{url('uploads/userImages/profilePhotos/'.$userDetails->picture)}}"
                                                 alt="" style="width: 120px; height: 120px; display: block;"/>
                                        </div>
                                        <div style="display: grid; text-align: center;" class="mt-3">
                                            <h4 class="m-t-sm"
                                                style="color: black">{{ $userDetails->first_name}} {{ $userDetails->last_name}}</h4>
                                            <p class="m-b-sm pl-2 p-2"
                                               style="color: black">{{ $userDetails->location}}</p>
                                        </div>

                                        <div class="text-center">
                                            <div class="">
                                                <a href="{{ route('user_tour_history',['user_id'=>$userDetails->user_id]) }}"
                                                   class=""
                                                   style="color: #ffffff !important;  font-weight: 700; background-color: #f8aa14; padding: 10px 30px; border-radius: 3px; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgb(0 0 0 / 32%);text-transform: uppercase; ">tour
                                                    history</a>
                                            </div>
                                            <div class="mt-4">
                                                <a href="{{ route('user_images',['user_id'=>$userDetails->user_id]) }}"
                                                   style="color: #ffffff !important;  font-weight: 700; background-color: #f8aa14; padding: 10px 50px; border-radius: 3px; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgb(0 0 0 / 32%);text-transform: uppercase; ">PHOTOS</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 pl-4 pr-4">
                                        <div class="row">
                                            <div class="col-md-6 hidden-xs hidden-sm">
                                                <ul class="profile-info-list">
                                                    <li>
                                                        <div class="field">Gender:</div>
                                                        <div class="value">{{ $userDetails->gender}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Sexual Orientation:</div>
                                                        <div class="value">{{ $userDetails->sexual_orientation}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Drinking Habits:</div>
                                                        <div class="value">{{ $userDetails->drinking_habits}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Smoker:</div>
                                                        <div class="value">{{ $userDetails->smoker}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Income:</div>
                                                        <div class="value">{{ $userDetails->income}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Weight:</div>
                                                        <div class="value">{{ $userDetails->weight}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Height:</div>
                                                        <div class="value">{{ $userDetails->height}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Build:</div>
                                                        <div class="value">{{ $userDetails->build}}</div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6 hidden-xs hidden-sm">
                                                <ul class="profile-info-list">
                                                    <!-- <li class="title">PERSONAL INFORMATION</li> -->
                                                    <li>
                                                        <div class="field">Email:</div>
                                                        <div class="value">{{ $userDetails->email}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Phone:</div>
                                                        <div class="value">{{ $userDetails->phone}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Address:</div>
                                                        <div class="value">{{ $userDetails->location}}</div>
                                                    </li>

                                                    <li>
                                                        <div class="field">Date of Birth:</div>
                                                        <div class="value">{{ $userDetails->birthday}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Language:</div>
                                                        <div class="value">{{ $userDetails->language}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Country:</div>
                                                        <div class="value">{{ $userDetails->user_country}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="field">Visited Country List:</div>
                                                        <div class="img-list">
                                                            @foreach($visited_country as $country)
                                                                <div>
                                                                    <a href="#" title="{{$country->name}}" class="m-b-5"><img class="rounded-circle visited_country_imgs"
                                                                                          src="{{ url('uploads/countryImages/'.$country->flag)}}"
                                                                                          alt=""
                                                                                          /></a>
                                                                </div>

                                                            @endforeach
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="content">
        <div class="container-fluid">
            <div class="container">
    <div id="content" class="content p-0">



        </div>
    </div>

        </div>
    </div>
     -->
@endsection
