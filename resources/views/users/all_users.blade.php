@extends('layouts.users')
@section('content')
 <div class="content">
        <div class="container-fluid">
              @include('pages.flash-message')
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0">All Users</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead class="">
                        <th>
                          Name
                        </th>
                        <th>
                          Email
                        </th>
                        <th>
                          Phone
                        </th>
                        <th>
                          Address
                        </th>
                        <th>
                          Status
                        </th>
                         <th>
                          Actions
                        </th>
                      </thead>
                      <tbody>
                        @foreach($users as $user)
                        <tr>
                          <td>{{$user->name}}</td>
                          <td>{{$user->email}}</td>
                          <td>{{$user->phone}}</td>
                          <td>{{$user->address}}</td>
                          <td>{{App\Models\Users::get_status($user->status)}}</td>
                          <td>
                           <!--  <a title="User Details" href="{{ route('user_details',['id'=>$user->id]) }}">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                            </a> -->
                            @if($user->status == '0')
                            <a href="{{ route('change_user_status',['id'=>$user->id, 'status'=>'1']) }}" title="Activate" class="change_status" message="Are you sure to activate this user?">
                              <i class="fa fa-ban"></i>
                            </a>
                            @endif
                            @if($user->status == '1')
                            <a href="{{ route('change_user_status',['id'=>$user->id, 'status'=>'0']) }}" title="DeActivate" class="change_status" message="Are you sure to DeActivate this user?">
                              <i class="fa fa-ban"></i>
                            </a>
                            @endif
                            
                            <a href="{{ route('change_user_status',['id'=>$user->id, 'status'=>'2']) }}" title="Delete" class="change_status" message="Are you sure to delete this user?">
                            <i class="fa fa-trash fa-2" aria-hidden="true"></i>
                          </a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection