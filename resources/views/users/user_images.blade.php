@extends('layouts.users')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title mt-0">User Images</h4>
                        </div>
                        <div class="card-body">
                            <div class="row text-center text-lg-left">
                                @foreach($userImages as $user)

                                    <div class="col-lg-3 col-md-4 col-6">
                                        <a href="#" class="d-block mb-4 h-100">
                                            <img class="img-fluid img-thumbnail" style="height: 15em;width: 15em" src="{{ url('uploads/userImages/'.$user->image)}}" alt="">
                                        </a>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
