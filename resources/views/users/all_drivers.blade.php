@extends('layouts.users')
@section('content')
 <div class="content">
        <div class="container-fluid">
    @include('pages.flash-message')
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0">All Drivers</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead class="">
                        <th>
                          Name
                        </th>
                        <th>
                          Email
                        </th>
                        <th>
                          Phone
                        </th>
                        <th>
                          Address
                        </th>
                        <th>
                          Status
                        </th>
                         <th>
                          Actions
                        </th>
                      </thead>
                      <tbody>
                        @foreach($drivers as $driver)
                        <tr>
                          <td>{{$driver->name}}</td>
                          <td>{{$driver->email}}</td>
                          <td>{{$driver->phone}}</td>
                          <td>{{$driver->address}}</td>
                          <td>{{App\Models\Users::get_status($driver->status)}}</td>
                          <td>
                           <!--  <a title="User Details" href="{{ route('user_details',['id'=>$driver->id]) }}">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                            </a> -->
                            @if($driver->status == '0')
                            <a href="{{ route('change_user_status',['id'=>$driver->id, 'status'=>'1']) }}" title="Activate" class="change_status" message="Are you sure to activate this driver?">
                              <i class="fa fa-ban"></i>
                            </a>
                            @endif
                            @if($driver->status == '1')
                            <a href="{{ route('change_user_status',['id'=>$driver->id, 'status'=>'0']) }}" title="DeActivate" class="change_status" message="Are you sure to DeActivate this driver?">
                              <i class="fa fa-ban"></i>
                            </a>
                            @endif
                            
                            <a href="{{ route('change_user_status',['id'=>$driver->id, 'status'=>'2']) }}" title="Delete" class="change_status" message="Are you sure to delete this driver?">
                            <i class="fa fa-trash fa-2" aria-hidden="true"></i>
                          </a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection