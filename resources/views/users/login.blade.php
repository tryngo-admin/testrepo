@extends('layouts.Login')
@section('content')
<form action="{{route('login_submitted')}}" method="post">
    @csrf
        <div class="logo"><a href="" class="simple-text logo-normal">
          <img src="{{ url('assets/img/logo (1).png') }}?v=1.0" style="height: auto; width: 160px">
        </a></div>       
        <div class="form-group">
            <input type="text" class="form-control" name="email" placeholder="Email" >
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
        <div class="clearfix">
            <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
            <a href="#" class="pull-right">Forgot Password?</a>
        </div>        
    </form>
    <p class="text-center"><a href="#">Create an Account</a></p>
    </div><!--card-->
@endsection
