@extends('layouts.restaurants')
@section('content')
 <div class="content">
        <div class="container-fluid">
    @include('pages.flash-message')
        	
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0">Add E-commerce</h4>
                </div>
          <div class="card-body">
                        <form action="{{route('ecommerce_submitted')}}" method="post" enctype="multipart/form-data" id="restaurant_form">
                            <input type="hidden" id="id" name="id" value="{{$ecommerce->id}}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title"> Name</label>
                                        <input type="text" class="form-control" value="{{$ecommerce->name}}" name="name" id="name">
                                    </div>
                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Phone</label>
                                        <input type="text" class="form-control" value="{{$ecommerce->phone}}" name="phone" id="phone">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title"> Address</label>
                                        <input type="text" class="form-control" value="{{$ecommerce->address}}" name="address" id="address">
                                    </div>
                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title"> Email</label>
                                        <input type="email" class="form-control" value="{{$ecommerce->email}}" name="email" id="email">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" id="add_ecommerce" class="btn btn-custom2 pull-right">Add</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
@endsection

