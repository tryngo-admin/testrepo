<div class="sidebar" data-color="yellow" data-background-color="white" data-image="{{url('assets/img/sidebar-1.jpg')}}">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"


        Tip 2: you can also add an image using data-image tag-->
      <div class="logo"><a href="" class="simple-text logo-normal">
          <img src="{{ url('assets/img/logo (1).png') }}?v=1.0" style="height: auto; width: 160px">
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item @if(isset($dashboard_tab)) {{'active'}} @endif">
            <a class="nav-link" href="{{ route('dashboard') }}">
              <i class="material-icons"></i>
              <p>Dashboard</p>
            </a>
          </li>
           <li class="nav-item @if (isset($users_tab)) {{'active'}} @endif">
            <a class="nav-link" href="{{ route('users') }}">
              <i class="material-icons"></i>
              <p>Users</p>
            </a>
          </li>
          <li class="nav-item @if (isset($driver_tab)) {{'active'}} @endif">
            <a class="nav-link" href="{{ route('drivers') }}">
              <i class="material-icons"></i>
              <p>Drivers</p>
            </a>
          </li>
          <li class="nav-item @if (isset($restaurants_tab)) {{'active'}} @endif">
            <a class="nav-link" href="{{ route('restaurants') }}">
              <i class="material-icons"></i>
              <p>Restaurants</p>
            </a>
          </li>
          <li class="nav-item @if (isset($ecommerce_tab)) {{'active'}} @endif">
            <a class="nav-link" href="{{ route('ecommerce') }}">
              <i class="material-icons"></i>
              <p>E-commerce</p>
            </a>
          </li>
        <!--   <li class="nav-item @if (isset($main_category_tab)) {{'active'}} @endif">
            <a class="nav-link" href="{{ route('main_categories') }}">
              <i class="material-icons"></i>
              <p>Main Categories</p>
            </a>
          </li> -->
          <li class="nav-item @if (isset($category_tab)) {{'active'}} @endif">
            <a class="nav-link" href="{{ route('categories') }}">
              <i class="material-icons"></i>
              <p>Categories</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('restaurant_products') }}">
              <i class="material-icons"></i>
              <p>Restaurant Products</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('ecommerce_products') }}">
              <i class="material-icons"></i>
              <p>Ecommerce Products</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('orders') }}">
              <i class="material-icons"></i>
              <p>Orders</p>
            </a>
          </li>
           <li class="nav-item @if (isset($profile_tab)) {{'active'}} @endif">
            <a class="nav-link" href="{{ route('profile') }}">
              <i class="material-icons"></i>
              <p>Profile</p>
            </a>
          </li>

        </ul>
      </div>
    </div>