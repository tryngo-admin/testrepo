 <script src="{{url('assets/js/jquery.min.js')}}"></script>
  <script src="{{url('assets/js/popper.min.js')}}"></script>
  <script src="{{url('assets/js/bootstrap-material-design.min.js')}}"></script>
  <script src="{{url('assets/js/perfect-scrollbar.jquery.min.js')}}"></script>
   <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{url('assets/js/material-dashboard.js?v=2.1.2')}}" type="text/javascript"></script>
  <script src="{{ url('assets/js/sweetalert2.min.js') }}"></script>

  <script type="text/javascript">
    //getting main category id for adding product
    let main_category_id = '';
  	$(document).ready(function function_name() {
            if (document.getElementById('product_id')) {
            main_category_id = $('#main_category_id').val();
    }else{
        main_category_id = '1';
    }
    //end

//show warning dialog when click on delete button
  		$(".change_status").click(function (event) {
            event.preventDefault();
            let msg = $(this).attr('message');
            let url = $(this).attr('href');
            Swal.fire({
                title: msg,
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#f8aa14',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    window.location.href = url;
                }
            })
        });
        //end

//display restaurant/ecommerce list on base of main_category_id selection  
//used for add/edit product , add/update category page
       $('#main_category').change(function () {
        main_category_id = '';
            main_category_id = this.value;
            console.log(main_category_id);
            $.ajax({
                method: "GET",
                url: "{{ route('restaurant_r_ecommerce') }}",
                data: {main_category_id: main_category_id}
            }).done(function (data) {
                $('#restaurant_r_ecommerce_id').html('');
                $('#restaurant_r_ecommerce_id').html(data.restaurant_r_ecommerce);
                $('#product_category_id').html('');
                $('#product_category_id').html(data.product_categories_list);

            });
        });
//display category list of restaurants/ecommerce on base of selection
//used for add/edit product
       $('#restaurant_r_ecommerce_id').change(function () {
            var restaurant_r_ecommerce_id = this.value;
            console.log(restaurant_r_ecommerce_id);
            $.ajax({
                method: "GET",
                url: "{{ route('restaurant_r_ecommerce_categories') }}",
                data: {restaurant_r_ecommerce_id: restaurant_r_ecommerce_id,
                  main_category_id: main_category_id}
            }).done(function (data) {
                $('#product_category_id').html('');
                $('#product_category_id').html(data);

            });
        });
       //end

//add/update product validation
       $('#add_product').click(function () {
            if ($('#main_category').val() == "") {
                $("#main_category").css("background-color", "#d6454dde");
                return false;
            } else if ($('#restaurant_r_ecommerce_id').val() == "") {
                $("#main_category").css("background-color", "#ffffff");
                $("#restaurant_r_ecommerce_id").css("background-color", "#d6454dde");
                return false;
            } else if ($('#product_category_id').val() == "") {
                $("#restaurant_r_ecommerce_id").css("background-color", "#ffffff");
                $("#product_category_id").css("background-color", "#d6454dde");
                return false;
            } else if ($('#product_name').val() == "") {
                $("#product_category_id").css("background-color", "#ffffff");
                $("#error_product_name").html('Product name is required');
                return false;
            } else if ($('#product_price').val() == "") {
                $("#error_product_name").html('');
                $("#error_product_price").html('Product price is required');
                return false;
            } else if (!document.getElementById('product_id') && $('#input').val() == "") {
                $("#error_product_price").html('');
                $("#error_product_name").html('');
                $("#error_product_image").html('Image is required');
                console.log('image is required');
                return false;
    }
        });

  	});

//show image when user chooses an image for upload
   function showImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatar')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
 </script>
  </script>