@extends('layouts.restaurants')
@section('content')
 <div class="content">
        <div class="container-fluid">
    @include('pages.flash-message')

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">

                      <div class="row">
                          <div class="col-md-6 mt-2">
                          <h4 class="card-title mt-0 align-middle">All Restaurants</h4>
                         </div>

                         <div class="col-md-6">
                          <a class="btn btn-sm pull-right float-right" href="{{ route('add_restaurant') }}"><span class="icon-space"><i class="fa fa-plus-circle" aria-hidden="true"></i></span>Add Restaurant</a>
                        </div>

                      </div>
                </div>

             <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead class="">
                        <th>
                          Name
                        </th>
                        <th>
                          Email
                        </th>
                        <th>
                          Phone
                        </th>
                        <th>
                          Address
                        </th>
                        <th>
                          Status
                        </th>
                         <th>
                          Actions
                        </th>
                      </thead>
                      <tbody>
                        @foreach($restaurants as $restaurant)
                        <tr>
                          <td>{{$restaurant->restaurant_name}}</td>
                          <td>{{$restaurant->email}}</td>
                          <td>{{$restaurant->phone}}</td>
                          <td>{{$restaurant->address}}</td>
                          <td>{{App\Models\Users::get_status($restaurant->status)}}</td>
                          <td>
                            <a href="{{route('edit_restaurant',['id'=>$restaurant->id])}}" title="Edit">
                            <i class="fa fa-edit"></i>
                            </a>
                          
                            @if($restaurant->status == '0')
                            <a href="{{ route('change_restaurant_status',['id'=>$restaurant->id, 'status'=>'1']) }}" title="Activate" class="change_status" message="Are you sure to activate this restaurant?">
                              <i class="fa fa-ban"></i>
                            </a>
                            @endif
                            @if($restaurant->status == '1')
                            <a href="{{ route('change_restaurant_status',['id'=>$restaurant->id, 'status'=>'0']) }}" title="DeActivate" class="change_status" message="Are you sure to DeActivate this restaurant?">
                              <i class="fa fa-ban"></i>
                            </a>
                            @endif
                            
                            <a href="{{ route('change_restaurant_status',['id'=>$restaurant->id, 'status'=>'2']) }}" title="Delete" class="change_status" message="Are you sure to delete this restaurant?">
                            <i class="fa fa-trash fa-2" aria-hidden="true"></i>
                          </a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

