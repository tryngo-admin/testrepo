@extends('layouts.restaurants')
@section('content')
 <div class="content">
        <div class="container-fluid">
    @include('pages.flash-message')
        	
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0">Add Restaurant</h4>
                </div>
          <div class="card-body">
                        <form action="{{route('restaurant_submitted')}}" method="post" enctype="multipart/form-data" id="restaurant_form">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" id="restaurant_id" value="">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title"> Name</label>
                                        <input type="text" class="form-control" name="restaurant_name" id="restaurant_name">
                                    </div>
                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Phone</label>
                                        <input type="text" class="form-control" name="restaurant_phone" id="restaurant_phone">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title"> Address</label>
                                        <input type="text" class="form-control" name="restaurant_address" id="restaurant_address">
                                    </div>
                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title"> Email</label>
                                        <input type="email" class="form-control" name="email" id="email">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" id="add_restaurant" class="btn btn-custom2 pull-right">Add</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
@endsection

