@extends('layouts.orders')
@section('content')
 <div class="content">
        <div class="container-fluid">
              @include('pages.flash-message')
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0">All Orders</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead class="">
                        <th>
                          User
                        </th>
                        <th>
                          Product Name
                        </th>
                        <th>
                          Category Name
                        </th>
                        <th>
                          quantity
                        </th>
                        <th>
                          Total Amount
                        </th>
                         <th>
                          Status
                        </th>
                      </thead>
                      <tbody>
                        @foreach($orders as $order)
                        <tr>
                          <td>{{$order->user_name}}</td>
                          <td>{{$order->product_name}}</td>
                          <td>{{$order->product_category_name}}</td>
                          <td>{{$order->quantity}}</td>
                          <td>{{$order->total_amount}}</td>
                          <td>{{App\Models\Orders::get_order_status($order->order_status)}}</td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection