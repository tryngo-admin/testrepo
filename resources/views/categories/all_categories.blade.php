@extends('layouts.categories')
@section('content')
 <div class="content">
        <div class="container-fluid">
      @include('pages.flash-message')
          
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <div class="row">
                          <div class="col-md-6 mt-2">
                          <h4 class="card-title mt-0 align-middle">All Categories</h4>
                         </div>

                         <div class="col-md-6">
                          <a class="btn btn-sm pull-right float-right" href="{{ route('add_category') }}"><span class="icon-space"><i class="fa fa-plus-circle" aria-hidden="true"></i></span>Add Category</a>
                        </div>

                      </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead class="">
                        <th>
                          Category Name
                        </th>
                        <th>
                          Main Category
                        </th>
                        <th>
                          Image
                        </th>
                        <th>
                          Status
                        </th>
                         <th>
                          Actions
                        </th>
                      </thead>
                      <tbody>
                        @foreach($categories as $c)
                        <tr>
                          <td>{{$c->product_category_name}}</td>
                          <td>{{$c->main_category_name}}</td>
                          <td><img class="justify-content-center expand" src="{{ url('uploads/categoryImages/'.$c->image) }}"/></td>
                          <td>{{App\Models\Users::get_status($c->status)}}</td>
                          <td>
                            <a href="{{ route('edit_category',['id'=>$c->id]) }}" title="Edir">
                            <i class="fa fa-edit"></i>
                            </a>
                          
                            @if($c->status == '0')
                            <a href="{{ route('change_category_status',['id'=>$c->id, 'status'=>'1']) }}" title="Activate" class="change_status" message="Are you sure to activate this category?">
                              <i class="fa fa-ban"></i>
                            </a>
                            @endif
                            @if($c->status == '1')
                            <a href="{{ route('change_category_status',['id'=>$c->id, 'status'=>'0']) }}" title="DeActivate" class="change_status" message="Are you sure to DeActivate this category?">
                              <i class="fa fa-ban"></i>
                            </a>
                            @endif
                            
                            <a href="{{ route('change_category_status',['id'=>$c->id, 'status'=>'2']) }}" title="Delete" class="change_status" message="Are you sure to delete this category?">
                            <i class="fa fa-trash fa-2" aria-hidden="true"></i>
                          </a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection