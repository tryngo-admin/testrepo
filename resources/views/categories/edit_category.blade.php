@extends('layouts.categories')
@section('content')
 <div class="content">
        <div class="container-fluid">
    @include('pages.flash-message')
        	
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0">Edit Category</h4>
                </div>
          <div class="card-body">
                        <form action="{{route('category_submitted')}}" method="post" enctype="multipart/form-data" id="restaurant_form">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" id="category_id" value="{{$category->id}}">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Select Main Category</label>
                                        <!-- <input type="text" class="form-control" name="restaurant_name" id="restaurant_name"> -->
                                        <select class="form-control" id="main_category" name="main_category">
                                            @foreach($main_categories As $cat)
                                            <option value="{{$cat->id}} " @if($category->main_category_id == $cat->id){{'selected'}} @endif>{{$cat->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Select Restaurant or Ecommerce</label>
                                        <!-- <input type="text" class="form-control" name="restaurant_name" id="restaurant_name"> -->
                                        <select class="form-control" id="restaurant_r_ecommerce_id" name="restaurant_r_ecommerce_id">
                                            @foreach($restaurants As $rat)
                                            <option value="{{$rat->id}} ">{{$rat->restaurant_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                               
                            </div>

                            <div class="row">
                                 <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Category Name</label>
                                        <input type="text" class="form-control" name="category_name" value="{{$category->product_category_name}}" id="category_name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <label for="slogan">Category Image</label>
                                    <div class="form-group">
                                        <label class="label" data-toggle="tooltip" title="Select Category image">
                                            @if($category->image !='')
                                            <img id="avatar" class="rounded" src="{{ url('uploads/categoryImages/'.$category->image) }}" alt="avatar" style="width: 120px;height: auto;cursor: pointer;">
                                            <input type="file" class="sr-only" id="input" name="image" onchange="showImage(this);" value="" accept="image/*">
                                            @else
                                            <img id="avatar" class="rounded" src="{{ url('assets/img/gallery_picture.png') }}" alt="avatar" style="width: 120px;height: auto;cursor: pointer;">
                                            <input type="file" class="sr-only" id="input" name="image" onchange="showImage(this);" value="" accept="image/*">
                                            @endif
                                            
                                        </label>
                                        <!-- <span class="validation-error" id="error_category_logo"></span> -->
                                    </div>
                                </div>
                            </div>
                            <button type="submit" id="" class="btn btn-custom2 pull-right">Update</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
@endsection

