@extends('layouts.categories')
@section('content')
 <div class="content">
        <div class="container-fluid">
    @include('pages.flash-message')
        	
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0">Edit Product</h4>
                </div>
          <div class="card-body">
                        <form action="{{route('product_submitted')}}" method="post" enctype="multipart/form-data" id="product_form">
                            {{ csrf_field() }}
                            <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}">
                            <input type="hidden" name="main_category_id" id="main_category_id" value="{{$main_category_id}}">
                            <div class="row">
                               
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label id="restaurant_r_ecommerce">Select Main Category</label>
                                        <!-- <input type="text" class="form-control" name="restaurant_name" id="restaurant_name"> -->
                                        <select class="form-control" class="" id="main_category" name="main_category">
                                            @foreach($main_categories As $cat)
                                            <option value="{{$cat->id}} " @if($main_category_id==$cat->id){{'selected'}}@endif>{{$cat->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Select Restaurant or Ecommerce</label>
                                        <!-- <input type="text" class="form-control" name="restaurant_name" id="restaurant_name"> -->
                                        <select class="form-control" id="restaurant_r_ecommerce_id" name="restaurant_r_ecommerce_id">
                                            @if($main_category_id ==='1')
                                            @foreach($restaurants As $rat)
                                            <option value="{{$rat->id}}"@if($rat->id == $product->resturant_id){{'selected'}}@endif>{{$rat->restaurant_name}}</option>
                                            @endforeach
                                            @endif
                                            @if($main_category_id ==='2')
                                            @foreach($ecommerce As $e)
                                            <option value="{{$e->id}}" @if($e->id == $product->ecommerce_store_id){{'selected'}}@endif>{{$e->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Select Product Category</label>
                                        <!-- <input type="text" class="form-control" name="restaurant_name" id="restaurant_name"> -->
                                        <select class="form-control" id="product_category_id" name="product_category_id" >
                                            @foreach($categories As $cat)
                                            <option value="{{$cat->id}}"@if($cat->id == $product->product_category_id){{'selected'}}@endif>{{$cat->product_category_name}}</option>
                                            @endforeach
                                           
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Product Name</label>
                                        <input type="text" class="form-control" name="product_name" value="{{$product->product_name}}" id="product_name">
                                    </div>
                                        <span class="validation-error" style="color: red" id="error_product_name"></span>

                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Product Price</label>
                                        <input type="number" class="form-control" value="{{$product->price}}" name="product_price" id="product_price">
                                    </div>
                                        <span class="validation-error" style="color: red" id="error_product_price"></span>

                                </div>
                            </div>
                            <div class="row">
                                 
                                <div class="col-md-12 pr-1">
                                    <div class="form-group">
                                        <label for="role_title">Product Description</label>
                                        <textarea class="form-control" name="description" id="description">{{$product->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <label for="slogan">Product Image</label>
                                    <div class="form-group">
                                        <label class="label" data-toggle="tooltip" title="Select Product image">
                                            <img id="avatar" class="rounded" src="{{ url('uploads/productImages/'.$product->product_image) }}" alt="avatar" style="width: 120px;height: auto;cursor: pointer;">
                                            <input type="file" class="sr-only" id="input" name="image" onchange="showImage(this);" value="" accept="image/*">
                                        </label>
                                        <span class="validation-error" style="color: red" id="error_product_image"></span>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" id="add_product" class="btn btn-custom2 pull-right">Update</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
@endsection

