@extends('layouts.restaurants')
@section('content')
 <div class="content">
        <div class="container-fluid">
      @include('pages.flash-message')
          
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <div class="row">
                          <div class="col-md-6 mt-2">
                          <h4 class="card-title mt-0 align-middle">All Restaurants Products</h4>
                         </div>

                         <div class="col-md-6">
                          <a class="btn btn-sm pull-right float-right" href="{{ route('add_product') }}"><span class="icon-space"><i class="fa fa-plus-circle" aria-hidden="true"></i></span>Add Product</a>
                        </div>

                      </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead class="">
                        <th>
                          Product Name
                        </th>
                        <th>
                          Restaurant
                        </th>
                        <th>
                          Category Name
                        </th>
                        <th>
                          Price
                        </th>
                        <th>
                          Image
                        </th>
                         <th>
                          Actions
                        </th>
                      </thead>
                      <tbody>
                        @foreach($products as $product)
                        <tr>
                          <td>{{$product->product_name}}</td>
                          <td>{{$product->restaurant_name}}</td>
                          <td>{{$product->product_category_name}}</td>
                          <td>{{$product->price}}</td>
                            
                          <td><img class="justify-content-center expand" src="{{ url('uploads/productImages/'.$product->product_image) }}"/></td>
                          <td>
                            <a href="{{route('edit_product',['main_category_id' =>$product->main_category_id,'product_id' => $product->id])}}" title="Edit">
                            <i class="fa fa-edit"></i>
                            </a>
                            
                            <a href="{{route('change_product_status',['main_category_id' =>$product->main_category_id,'product_id' => $product->id,'status'=>'2'])}}" title="Delete" class="change_status" message="Are you sure to delete this product?">
                            <i class="fa fa-trash fa-2" aria-hidden="true"></i>
                          </a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection