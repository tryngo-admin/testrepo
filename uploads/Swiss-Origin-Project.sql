
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `barcode` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `product_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `brand` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `trademark_number` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `product_photo` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `nutriscore` TEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `certification_status` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` boolean COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `request` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `temp_products`;
CREATE TABLE `temp_products` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `barcode` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `product_name` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `trademark_number` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_photo` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `nutriscore` TEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `certification_status` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` boolean COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `allergens`;
CREATE TABLE `allergens` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `barcode` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `allergens` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` boolean COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `nutriments`;
CREATE TABLE `nutriments` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `barcode` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `carbohydrates` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `carbohydrates_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `nutrition_score_fr` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `nova_group_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_kcal_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiber_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sodium_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiber_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sugars_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `saturated_fat_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `saturated_fat_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `nova_group` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_kcal` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `proteins` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sugars_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiber_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sodium_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `proteins_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `nutrition_score_fr_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sugars_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fat_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiber` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_kcal_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_kcal_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sodium_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sugars` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fruits_vegetables_nuts_estimate_from_ingredients_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `proteins_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `proteins_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sodium` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `nutrition_score_fr_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fat_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `saturated_fat` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sugars_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `carbohydrates_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fat_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `saturated_fat_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `energy_kcal_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `sodium_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `nova_group_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiber_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `proteins_serving` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `saturated_fat_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fat_unit` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `carbohydrates_value` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `carbohydrates_100g` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `fat` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` boolean COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `product_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` int COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` boolean COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `searched_history`;
CREATE TABLE `searched_history` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `product_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `status` boolean COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `user_favourites`;
CREATE TABLE `user_favourites` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `user_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `first_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `last_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `email` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` boolean COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `mobilesessions`;
CREATE TABLE `mobilesessions` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `user_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_date` datetime COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `mobileemailotp`;
CREATE TABLE `mobileemailotp` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `user_id` int(11) NOT NULL,
  `tokenforemail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_date` datetime COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `userroles`;
CREATE TABLE `userroles` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `role_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `status` boolean COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `userroles`(
    `role_name`,
    `status`,
    `updated_at`,
    `created_at`
)
VALUES(
    "superadmin",
    1,
    "2019-10-14",
    "2019-10-14"
);

INSERT INTO `userroles`(
    `role_name`,
    `status`,
    `updated_at`,
    `created_at`
)
VALUES(
    "admin",
    1,
    "2019-10-14",
    "2019-10-14"
);
INSERT INTO `userroles`(
    `role_name`,
    `status`,
    `updated_at`,
    `created_at`
)
VALUES(
    "monitor",
    1,
    "2019-10-14",
    "2019-10-14"
);
