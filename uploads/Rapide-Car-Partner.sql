DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `email` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` LONGTEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` boolean COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `mobilesessions`;
CREATE TABLE `mobilesessions` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `user_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_date` datetime COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `mobileemailotp`;
CREATE TABLE `mobileemailotp` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `user_id` int(11) NOT NULL,
  `tokenforemail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_date` datetime COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `userroles`;
CREATE TABLE `userroles` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `role_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `status` boolean COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `userroles`(
    `role_name`,
    `status`,
    `updated_at`,
    `created_at`
)
VALUES(
    "superadmin",
    1,
    "2019-10-14",
    "2019-10-14"
);

INSERT INTO `userroles`(
    `role_name`,
    `status`,
    `updated_at`,
    `created_at`
)
VALUES(
    "driver",
    1,
    "2019-10-14",
    "2019-10-14"
);
INSERT INTO `userroles`(
    `role_name`,
    `status`,
    `updated_at`,
    `created_at`
)
VALUES(
    "user",
    1,
    "2019-10-14",
    "2019-10-14"
);
