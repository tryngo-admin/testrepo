-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2020 at 10:00 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swissoriginapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `barcode` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `trademark_number` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_photo` longtext COLLATE utf8mb4_unicode_ci,
  `nutriscore` text COLLATE utf8mb4_unicode_ci,
  `certification_status` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `barcode`, `product_name`, `brand`, `trademark_number`, `product_photo`, `certification_status`, `status`, `created_at`, `updated_at`) VALUES
(1, '7610200258984', 'Sonnen blumenol', 'Migros', '3P-268357', '', 'Not Swiss Made but packed in Switzerland.', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(2, '7610200373311', 'Valflora Milch', 'Valflora', '2P-298867', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(3, '7617067014708', 'Gruyère', 'Gruyère', '07382/2019', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(4, '8000795011320', 'Fonte Tavina', 'Tavina', '646578', '', 'No', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(5, '7610994455811', 'Bimbosan', 'Bimbosan', '628852', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(6, '7610029179569', 'Noir 50% Cacao', 'Denner ', '2P-419070', '', 'Only Packed in Switzerland', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(7, '4023161042531', 'Bierhefe', 'Eubiona', 'No protected', '', 'No', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(8, '7616700246438', 'Steinhofen Baguette', 'Migros ', '3P-268357', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(9, '7610039000020', 'Speisesalz', 'Jurasel', 'P-484430', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(10, '7613030839828', 'Caramelita', 'Moevenpick', '3P-301899', '', 'No', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(11, '7610200337450', 'Natürliches Mineralwasser mit Kohlensäure', 'Migros', '3P-268357', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(12, '76159183', 'Cenovis (condiment salé á tartiner) - 70g', 'Cenovis', 'P-369604', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(13, '7610278000089', 'aarberg', 'Aarberg', 'No protected', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(14, '5410126446029', 'Speculo0s', 'Speculoos ', '65126', '', 'No', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(15, '7616500660014', 'Milch extra', 'Migros Frey ', 'P-500191', '', 'No', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(16, '4011100015470', 'M&M', 'M&M', 'P-375025', '', 'No', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(17, '7612100017487', 'ovomaltine', 'ovomaltine', 'P-377527', '', 'Swiss Origin', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(18, '7613312297117', 'Party honey nuts', 'Migros Party', '2P-323458', '', 'Only Packed in Switzerland', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(19, '7613312073766', 'Premium Noix De Pecan - Sun Queen', 'Migros Sun Queen', 'No protected', '', 'Only Packed in Switzerland', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19'),
(20, '7613269333920', 'The Vert Bio Migros', 'Migros', '3P-268357', '', 'Only Packed in Switzerland', 1, '2020-06-30 02:13:19', '2020-06-30 02:13:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
